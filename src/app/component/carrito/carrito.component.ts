import { Component, OnInit } from '@angular/core';
import { CarritoService } from 'src/app/service/carrito.service';
import { ProductosService } from 'src/app/service/productos.service';
import { ModalCompraComponent } from '../modal-compra/modal-compra.component';
import { NgbModal,NgbActiveModal  } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {
  productosCarrito=[];
  total:number=0;

  constructor(private carritoService:CarritoService, private productosService:ProductosService,  private modalService: NgbModal) { }

  ngOnInit(): void {
    this.productosCarrito=this.carritoService.getListaProductosCarrito();
  }

  eliminarProductoCarrito(producto){
    this.carritoService.eliminarProductoCarrito(producto);
    let productoActualizar = this.productosService.getProductoById(producto.idProducto);
    this.productosService.actualizarStock(productoActualizar.id,productoActualizar.stock+1);
  }

  finalizarCompra() {
    this.modalService.open(ModalCompraComponent);
  }

  getTotal() {
    this.total = this.carritoService.getTotal();
    return this.total;
  }

}