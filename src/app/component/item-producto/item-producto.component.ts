import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-item-producto',
  templateUrl: './item-producto.component.html',
  styleUrls: ['./item-producto.component.css']
})
export class ItemProductoComponent implements OnInit {
  @Input() producto=null;
  @Output() aniadirCarrito = new EventEmitter();  

  constructor() { }

  ngOnInit(): void {
  }

  aniadir(): void {
    this.aniadirCarrito.emit();
  }

}
