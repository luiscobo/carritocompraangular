import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductosService } from 'src/app/service/productos.service';

@Component({
  selector: 'app-modal-producto',
  templateUrl: './modal-producto.component.html',
  styleUrls: ['./modal-producto.component.css']
})
export class ModalProductoComponent implements OnInit {
  @Input() id: number;
  formNuevoProducto: FormGroup;
  categorias=[];
  selected;
  imageBase64: any;
  
  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder,private productoService:ProductosService) { 
    this.createForm();
  }

  ngOnInit(): void {
    this.categorias = this.productoService.getListaCategorias();
    this.selected = this.categorias[0].nombre;
  }

  createForm() {
    this.formNuevoProducto = this.formBuilder.group({
      nombreProducto: null,
      descripcionProducto: null,
      precioProducto: null,
      stockProducto: null,
      categoriaProducto: null,
      imgProducto: null
    });
  }

  guardarProducto() {
    console.log(this.formNuevoProducto);
    this.activeModal.close();
    const productoForm = this.formNuevoProducto.value;
    let productoNuevo = {
        id: null,
        nombre: productoForm.nombreProducto,
        descripcion: productoForm.descripcionProducto,
        categoria: productoForm.categoriaProducto,
        precio: productoForm.precioProducto,
        stock: productoForm.stockProducto,
        imagen:  this.imageBase64
    }
    this.productoService.guardarProducto(productoNuevo);
  }

  // Función para obtener la img en base64
  fileChangeEvent(fileInput: any){
    this.imageBase64 =null;
    const reader = new FileReader();
    reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {    
          this.imageBase64 = e.target.result;         
        };
    };
    reader.readAsDataURL(fileInput.target.files[0]);    
  }

}
