import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/service/productos.service';
import { ModalProductoComponent } from '../modal-producto/modal-producto.component';
import { ModalCategoriaComponent } from '../modal-categoria/modal-categoria.component';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  productos= [];
  page = 1;
  pageSize = 7;
  collectionSize = 0;
  categorias=[];

  constructor(private productoService:ProductosService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.refrescarProductos();
    this.categorias=this.productoService.getListaCategorias();
  }

  nuevoProducto() { 
    const modalRef = this.modalService.open(ModalProductoComponent);
    modalRef.componentInstance.id = 1; 
    
    modalRef.result.then((result) => {
      //Actualizamos el listado de productos
      this.refrescarProductos();
    }).catch((error) => {
      console.log(error);
    });
    
  }

  nuevaCategoria() { 
    const modalRef = this.modalService.open(ModalCategoriaComponent);
    modalRef.componentInstance.id = 2;
    
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
  }

  refrescarProductos() {
    const productosSer=this.productoService.getListaProductos();
    console.log(productosSer);
    this.collectionSize = productosSer.length;
    this.productos = productosSer
      .map((producto, i) => ({id: i + 1, ...producto}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

}
