import { Component, OnInit } from '@angular/core';
import { ProductosService } from 'src/app/service/productos.service';
import { CarritoService } from 'src/app/service/carrito.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {
  productos = null;
  categorias = null;

  constructor( private productoService:ProductosService, private carritoService:CarritoService) { }

  ngOnInit(): void {
   this.categorias=this.productoService.getListaCategorias();
   this.productos=this.productoService.getListaProductosByCategoria(this.categorias[0].nombre);  
  }

  aniadir(producto){
    this.carritoService.agregarProducto(producto);
    this.productoService.actualizarStock(producto.id,producto.stock-1)
  }

  seleccionCategoria(cat) {
    this.productos=this.productoService.getListaProductosByCategoria(cat);
  }

}
