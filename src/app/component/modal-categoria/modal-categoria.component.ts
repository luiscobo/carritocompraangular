import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProductosService } from 'src/app/service/productos.service';

@Component({
  selector: 'app-modal-categoria',
  templateUrl: './modal-categoria.component.html',
  styleUrls: ['./modal-categoria.component.css']
})
export class ModalCategoriaComponent implements OnInit {
  @Input() id: number;
  formNuevaCategoria: FormGroup;

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder,private productoService:ProductosService) { 
    this.createForm();
  }

  createForm() {
    this.formNuevaCategoria = this.formBuilder.group({
      nombreCategoria: null     
    });
  }

  ngOnInit(): void {
  }

  guardarCategoria() {    
    this.activeModal.close();
    const categoriaForm = this.formNuevaCategoria.value;    
    this.productoService.guardarCategoria(categoriaForm.nombreCategoria);
  }

}
