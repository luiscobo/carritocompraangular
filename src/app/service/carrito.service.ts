import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {
  productosCarrito = [];
  total:number = 0;

  constructor() { }

  getListaProductosCarrito(){
    return this.productosCarrito;
  }
  
  agregarProducto(producto) {
    // Comprobamos si ya existe el producto en el carrito.
    let productoActual = this.productosCarrito.find(p=>p.idProducto==producto.id);

    if(productoActual==null){
      let productoNuevo = {
        idProducto: producto.id,
        nombreProducto: producto.nombre,
        imgProducto: producto.imagen,
        cantidadProducto: 1,
        precioProducto: producto.precio,
        totalProducto: producto.precio
      }  
      this.productosCarrito.push(productoNuevo);  
        
    } else {
      productoActual.cantidadProducto++;
      productoActual.totalProducto=productoActual.precioProducto*productoActual.cantidadProducto;
    }   
    // Actualizamos el total
    this.total = this.total+producto.precio

  }

  eliminarProductoCarrito(producto){
    let productoEliminar = this.productosCarrito.find(p=>p.idProducto==producto.idProducto);

    if(productoEliminar.cantidadProducto==1){
      // Buscamos el indice del elemento
      let index = this.productosCarrito.findIndex(p => p == producto.idProducto);
      // Eliminamos el elemento
      this.productosCarrito.splice(index,1);     
    } else{
      productoEliminar.cantidadProducto=productoEliminar.cantidadProducto-1;
    }
    // Actualizamos el total
    this.total = this.total-producto.precioProducto


  }

  getTotal():number { 
    return this.total;    
  }

}
