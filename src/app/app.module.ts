import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductosComponent } from './component/productos/productos.component';
import { ItemProductoComponent } from './component/item-producto/item-producto.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CarritoComponent } from './component/carrito/carrito.component';
import { ModalCompraComponent } from './component/modal-compra/modal-compra.component';
import { AdminComponent } from './component/admin/admin.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalProductoComponent } from './component/modal-producto/modal-producto.component';
import { ModalCategoriaComponent } from './component/modal-categoria/modal-categoria.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    ItemProductoComponent,
    CarritoComponent,
    ModalCompraComponent,
    AdminComponent,
    ModalProductoComponent,
    ModalCategoriaComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    NgbPaginationModule,
    FormsModule,
    ReactiveFormsModule   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
